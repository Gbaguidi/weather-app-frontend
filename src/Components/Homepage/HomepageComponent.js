import './HomepageComponent.css';
import {useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMagnifyingGlass,faSpinner } from '@fortawesome/free-solid-svg-icons'

function SearchButton(){
  return <FontAwesomeIcon className="search-icon" icon={faMagnifyingGlass} />
}
function Loader(props){
  return (
    <div className="loader">
      <FontAwesomeIcon className="spinner-icon" icon={faSpinner} />
    </div>
  )
}

function Homepage(props) {

  const [location,setLocation]=useState("")
  const [isloading,setIsloading]=useState(false)
  
  const handleSubmit=async(e)=>{
    e.preventDefault()
    setIsloading(true)
    await props.getGeolocation(location)
    setIsloading(false)
  }

  return (
    <>
      <div className='default container'>
        <p className='input-title'>
          <span className='green-medium'>The Forecast</span>
          <br/>
          <span className='green-light'>Weather App</span>
        </p>
        <div className='form-container'>
          <input className="location-input" type="text" name="location" value={location} onChange={(e)=>{
            setLocation(e.target.value)
          }} required placeholder="Search"/>
          <button type="submit" className="submit-location" onClick={handleSubmit} disabled={location===""}>
            <SearchButton/>
          </button>
        </div>
        {isloading&&<Loader/>}
      </div>
    </>
  );
}

export default Homepage;
