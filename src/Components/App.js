import './App.css';
import { ToastContainer,toast } from 'react-toastify';
import {useState} from 'react';
import {getFlags, getGeolocation,getWeathersInfo} from '../Request/request';
import HomePage from './Homepage/HomepageComponent'
import WeathersPage from './WeathersPage/WeathersPageComponent'

function App() {
  const [showHomepage,setShowHomepage]=useState(true)
  const [weatherInfo,setWeatherInfo]=useState({})
  const [backgroundPicture,setBackgroundPicture]=useState("")
  const [locationNotFound,setLocationNotFound]=useState(false)


  const returnHome = ()=>{
    setShowHomepage(true)
  }

  const handleFormSubmit=async(location)=>{
    let geolocation = await getGeolocation(location)
    if (geolocation != null ) {
      setLocationNotFound(false)
      let weathers= await getWeathersInfo(geolocation) 
      let flags= await getFlags(geolocation.country)
      
      setBackgroundPicture(flags)
      setWeatherInfo(weathers)   
      setShowHomepage(false)

    }else{
      toast('Aucune correspondances avec la/le ville/Pays entre.', {
        position: "top-right",
        autoClose: 2500,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      setLocationNotFound(true)
      setShowHomepage(true)


    }
  }

  return (
    <div className="app">
      {showHomepage&&<HomePage getGeolocation={handleFormSubmit} locationNotFound={locationNotFound}/>}
      {!showHomepage&&<WeathersPage backgroundPicture={backgroundPicture} returnHome={returnHome} weatherInfos={weatherInfo}/>}
      <ToastContainer />
    </div>
  );
}

export default App;
