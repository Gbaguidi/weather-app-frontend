import './WeatherDetailsCardComponent.css';
import { useEffect, useState } from 'react';

function WeatherDetailsCard(props) {
  const [weather,setWeather]=useState(props.weather)

  useEffect(()=>{
    setWeather(props.weather)
  },[props.weather])

  return (
    <div className='weather-details-card shadow' >
      <div className='card-header'>
        <img alt={weather.icon.description} src={`${process.env.REACT_APP_OPENWEATHERMAP_IMAGE_URL_}img/wn/${weather.icon.name}.png`}/>
        <span className='title'>{weather.date}</span>
      </div>
      <div className='card-body'>
        <div>
          <p> Jour- {weather.tempDay}° C</p>
          <p> Nuit- {weather.tempNight}° C</p>
          <p> Humidité- {weather.humidity} %</p>
        </div>
        <div>
          <p> Pression- {weather.pressure} hPa</p>
          <p> Vent- {weather.wind} Km/h</p>
        </div>
      </div>
    </div>
  );
}

export default WeatherDetailsCard;
