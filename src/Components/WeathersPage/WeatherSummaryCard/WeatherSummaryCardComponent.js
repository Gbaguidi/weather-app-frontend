import './WeatherSummaryCardComponent.css';
import {useState} from 'react';
import {getDay,getDate} from '../../../Request/utils';

function WeatherSummaryCard(props) {
  
  const [weatherInfo,setWeatherInfo]=useState(props.weatherInfo)

  return (
    <div  className='weather-summary-card shadow' onClick={()=>{props.changeSelectedDay(props.id)}}>
      <img alt={weatherInfo.icon.description} src={`${process.env.REACT_APP_OPENWEATHERMAP_IMAGE_URL_}img/wn/${weatherInfo.icon.name}.png`}/>
      <div>
        <span className='day-title'> {getDay(weatherInfo.date)} </span>
        <br/>
        <span className='date'>{getDate(weatherInfo.date)}</span>
      </div>
      <span className='temperature'> {weatherInfo.temp}°C</span>
    </div>
  );
}

export default WeatherSummaryCard;
