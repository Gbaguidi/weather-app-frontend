import './WeathersPageComponent.css';
import {useState} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeftLong } from '@fortawesome/free-solid-svg-icons'
import WeatherSummaryCard from './WeatherSummaryCard/WeatherSummaryCardComponent'
import WeatherDetailsCard from './WeatherDetailsCard/WeatherDetailsCardComponent'

function PreviousArrow(){
  return(<FontAwesomeIcon icon={faArrowLeftLong} height="70%" />)
}

function WeathersPage(props) {

  const [weatherInfos,setWeatherInfos]=useState(props.weatherInfos)
  const [selectedDay,setSelectedDay]=useState(weatherInfos[0])

  const changeSelectedDay=(id)=>{
    if (weatherInfos[id].date === selectedDay.date) {
      setSelectedDay(weatherInfos[0])
    }else{
      setSelectedDay(weatherInfos[id])
    }
  }

  return (
    <div className='weather-container'  style={{backgroundImage: `url(${props.backgroundPicture})`}}>
      <div className='weather-details-container'>
        <button className='prev-button shadow' onClick={()=>{
          props.returnHome()
        }}>
          <PreviousArrow/>
        </button>
        <WeatherDetailsCard weather={selectedDay}/>
      </div>
      <div className='weather-summary-list'>
        {
          weatherInfos.map((entry,key)=>(
            key!==0&&
            <WeatherSummaryCard key={key} id={key} weatherInfo={entry} changeSelectedDay={changeSelectedDay}/>
          ))
        }
      </div>
    </div>
  );
}

export default WeathersPage;
