import axios from "axios"


const getGeolocation=async(location)=>{
    let geolocation= await axios.get(`${process.env.REACT_APP_PROXY_API_URL}geolocation/${location}`)
    .then(
        async(response)=>{
            // console.log(response)
            return response.data.location
        }
    ).catch(
        async(error)=>{
            console.log(error)
            return {"status":"error",message:error}
        }
    )
    return geolocation
}

const getFlags=async(countryCode)=>{
    let geolocation= await axios.get(`${process.env.REACT_APP_PROXY_API_URL}get-flag/${countryCode}`)
    .then(
        async(response)=>{
            // console.log(response.data.flag)
            return response.data.flag
        }
    ).catch(
        async(error)=>{
            console.log(error)
            return {"status":"error",message:error}
        }
    )
    return geolocation
}
const getWeathersInfo=async(geolocation)=>{
    let weathersInfo= await axios.get(`${process.env.REACT_APP_PROXY_API_URL}get-wheather/${geolocation.lon}/${geolocation.lat}`)
    .then(
        async(response)=>{
            // console.log(response.data.wheather)
            return response.data.wheather
        }
    ).catch(
        async(error)=>{
            console.log(error)
            return {"status":"error",message:error}
        }
    )
    return weathersInfo
}

export {getGeolocation,getWeathersInfo,getFlags}