# Exercice 1

Temps de réalisation : 4h  
2H Pour l'implémentation  
2H Pour la responsivitée


## Setup the project
- Cloner le projet avec la commande : 
```
    git clone git@gitlab.com:Gbaguidi/weather-app-frontend.git
```
- Déplacer vous dans le répertoire du projet :
```
    cd weather-app-frontend
```
- Installer les dépendances avec npm :
```
    npm install
``` 
- Utiliser le fichier **.env.dev** pour créer le fichier **.env** :
```
    cp .env.dev .env
``` 
- Lancer l'application avec la commande :
``` 
npm start
```

## Principes utilisés

- Un répertoire racine pour tout les components avec comme point d'entrer le fichier App.js
- Les components ou groupe de component sont rassembler dans le même répertoire suivant leur arborescence
- Chaque dossier contient les fichiers js et css propre à chaque component
- Un répertoire racine pour toutes les requêtes (vu le nombre de requêtes, nous avons utilisé un seul fichier est suffisant.) 
- Préférer les component fonction au component classe pour faciliter la compréhension du code et éviter les ambiguïtés propres aux component de type classe
- Rendre le code modulaire en décomposant l'interface en plusieurs components suivant la logique propre à chaque élément
- Préférer les fichiers des .css au style inline.
## Recommandations

- Rajouter de l'autocomplétion en utilisant l'ensemble des correspondances avec le/la pays/ville renseignée ;
- Différencier le traitement des requêtes en deux cas, pays et ville. Pour le cas des pays, on pourrait faire un traitement supplémentaire en proposant les principales villes du pays dans le retour en autocomplétion.
- Revoir la responsivité et mieux structurer le CSS  